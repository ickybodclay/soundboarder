package com.brokenshotgun.soundboarder.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.brokenshotgun.soundboarder.R;
import com.brokenshotgun.soundboarder.models.SoundCue;
import com.brokenshotgun.soundboarder.sound.SoundManager;
import com.brokenshotgun.soundboarder.util.KeyUtils;
import com.brokenshotgun.soundboarder.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends ActionBarActivity {
    public static final String TAG = MainActivity.class.getName();

    private static final int ASSIGN_SOUND_REQUEST = 1;
    private static final int EDIT_SOUND_REQUEST = 2;

    private static final int SOUND_BUTTON_COUNT   = 20;

    @Bean
    SoundManager soundManager;

    @ViewById(R.id.stop) Button stopButton;
    @ViewById Button button11;
    @ViewById Button button12;
    @ViewById Button button13;
    @ViewById Button button14;
    @ViewById Button button21;
    @ViewById Button button22;
    @ViewById Button button23;
    @ViewById Button button24;
    @ViewById Button button31;
    @ViewById Button button32;
    @ViewById Button button33;
    @ViewById Button button34;
    @ViewById Button button41;
    @ViewById Button button42;
    @ViewById Button button43;
    @ViewById Button button44;
    @ViewById Button button51;
    @ViewById Button button52;
    @ViewById Button button53;
    @ViewById Button button54;

    @ViewById(R.id.assignKeyOverlay)
    TextView assignKeyOverlay;

    @OptionsMenuItem(R.id.action_toggle_edit)
    MenuItem toggleEditItem;

    private Gson gson;
    private SoundCue[] cueArray;
    private final Map<Integer, Integer> keyBindings = new HashMap<>();
    private int assignIndex;
    private int assignButtonIndex;
    private boolean isEditMode = true;
    private boolean loadOnStart = true;

    @AfterViews
    void init() {
        setButtonColors();

        cueArray = new SoundCue[SOUND_BUTTON_COUNT];
        for(int i=0; i< cueArray.length; ++i) {
            deleteSound(i);
        }

        gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriTypeAdapter())
                .create();

        if(loadOnStart) {
            loadLayout();
        }
    }

    private void setButtonColors() {
        //red
        stopButton.getBackground().setColorFilter(0xFFee4035, PorterDuff.Mode.MULTIPLY);

        // orange
        button11.getBackground().setColorFilter(0xFFf37736, PorterDuff.Mode.MULTIPLY);
        button12.getBackground().setColorFilter(0xFFf37736, PorterDuff.Mode.MULTIPLY);
        button13.getBackground().setColorFilter(0xFFf37736, PorterDuff.Mode.MULTIPLY);
        button14.getBackground().setColorFilter(0xFFf37736, PorterDuff.Mode.MULTIPLY);

        // yellow
        button21.getBackground().setColorFilter(0xFFfdf498, PorterDuff.Mode.MULTIPLY);
        button22.getBackground().setColorFilter(0xFFfdf498, PorterDuff.Mode.MULTIPLY);
        button23.getBackground().setColorFilter(0xFFfdf498, PorterDuff.Mode.MULTIPLY);
        button24.getBackground().setColorFilter(0xFFfdf498, PorterDuff.Mode.MULTIPLY);

        // green
        button31.getBackground().setColorFilter(0xFF7bc043, PorterDuff.Mode.MULTIPLY);
        button32.getBackground().setColorFilter(0xFF7bc043, PorterDuff.Mode.MULTIPLY);
        button33.getBackground().setColorFilter(0xFF7bc043, PorterDuff.Mode.MULTIPLY);
        button34.getBackground().setColorFilter(0xFF7bc043, PorterDuff.Mode.MULTIPLY);

        // blue
        button41.getBackground().setColorFilter(0xFF0392cf, PorterDuff.Mode.MULTIPLY);
        button42.getBackground().setColorFilter(0xFF0392cf, PorterDuff.Mode.MULTIPLY);
        button43.getBackground().setColorFilter(0xFF0392cf, PorterDuff.Mode.MULTIPLY);
        button44.getBackground().setColorFilter(0xFF0392cf, PorterDuff.Mode.MULTIPLY);

        //purple
        button51.getBackground().setColorFilter(0xFF6c2a7d, PorterDuff.Mode.MULTIPLY);
        button52.getBackground().setColorFilter(0xFF6c2a7d, PorterDuff.Mode.MULTIPLY);
        button53.getBackground().setColorFilter(0xFF6c2a7d, PorterDuff.Mode.MULTIPLY);
        button54.getBackground().setColorFilter(0xFF6c2a7d, PorterDuff.Mode.MULTIPLY);
    }

    @OptionsItem(R.id.action_toggle_edit)
    void toggleEditClicked() {
        isEditMode = !isEditMode;

        toggleEditItem.setTitle("View mode: " + (isEditMode ? "[EDITING]" : "[VIEWING]"));
    }

    @OptionsItem(R.id.action_save_layout)
    void saveLayout() {
        SharedPreferences prefs = getSharedPreferences("soundboarder", MODE_PRIVATE);
        String layoutJson = gson.toJson(cueArray, SoundCue[].class);
        prefs.edit()
            .putString("layout", layoutJson)
            .apply();
        Toast.makeText(this, "Saved layout.", Toast.LENGTH_SHORT).show();
    }

    @OptionsItem(R.id.action_load_layout)
    void loadLayout() {
        SharedPreferences prefs = getSharedPreferences("soundboarder", MODE_PRIVATE);
        if(prefs.contains("layout")) {
            cueArray = gson.fromJson(prefs.getString("layout", "[]"), SoundCue[].class);
            soundManager.load(cueArray);
            refreshKeyBindings();
            for(int i=0; i<SOUND_BUTTON_COUNT; ++i) {
                updateButtonText(i);
            }
            Toast.makeText(this, "Loaded layout.", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, "No saved layout found.", Toast.LENGTH_SHORT).show();
    }

    @Click(R.id.stop)
    void onStopClicked() {
        soundManager.stop();

        for(int i=0; i<SOUND_BUTTON_COUNT; ++i) {
            changeTextColor(i, 0xffcc2c00);
        }
    }

    @Click( {
        R.id.button11, R.id.button12, R.id.button13, R.id.button14,
        R.id.button21, R.id.button22, R.id.button23, R.id.button24,
        R.id.button31, R.id.button32, R.id.button33, R.id.button34,
        R.id.button41, R.id.button42, R.id.button43, R.id.button44,
        R.id.button51, R.id.button52, R.id.button53, R.id.button54 })
    void onSoundButtonClicked(View button) {
        int i = getIndexForButton(button);
        if(i != -1) {
            if(isEditMode)
                assignOrPlay(i);
            else
                play(i);
        }
    }

    @LongClick({
            R.id.button11, R.id.button12, R.id.button13, R.id.button14,
            R.id.button21, R.id.button22, R.id.button23, R.id.button24,
            R.id.button31, R.id.button32, R.id.button33, R.id.button34,
            R.id.button41, R.id.button42, R.id.button43, R.id.button44,
            R.id.button51, R.id.button52, R.id.button53, R.id.button54 })
    void onSoundButtonLongClicked(View button) {
        int i = getIndexForButton(button);
        if (i != -1) {
            if (isEditMode && cueArray[i] != null) {
                showSoundOptionDialog(i);
            }
        }
    }

    private void showSoundOptionDialog(final int buttonIndex) {
        Dialog dialog = new AlertDialog.Builder(this)
            .setTitle("Cue Options")
            .setItems(StringUtils.snakeCaseToSentenceCase(CueOption.names()), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (CueOption.values()[which]) {
                        case EDIT_CUE_OPTIONS:
                            editSoundOptions(buttonIndex);
                            break;
                        case ASSIGN_KEY:
                            assignKey(buttonIndex);
                            updateButtonText(buttonIndex);
                            break;
                        case ADD_TRIGGERS:
                            addTriggers(buttonIndex);
                            break;
                        case DELETE_CUE:
                            deleteSound(buttonIndex);
                            updateButtonText(buttonIndex);
                            break;
                    }
                }
            })
            .create();
        dialog.show();
    }

    private void editSoundOptions(int buttonIndex) {
        EditCueActivity_
            .intent(this)
            .cue(cueArray[buttonIndex])
            .index(buttonIndex)
            .startForResult(EDIT_SOUND_REQUEST);
    }

    private void assignKey(int buttonIndex) {
        assignKeyOverlay.setVisibility(View.VISIBLE);
        assignButtonIndex = buttonIndex;
    }

    private ArrayList<Integer> mSelectedItems;
    private String[] triggers = new String[] {
        "BUTTON#1",
        "BUTTON#2",
        "BUTTON#3",
        "BUTTON#4",
        "BUTTON#5",
        "BUTTON#6",
        "BUTTON#7",
        "BUTTON#8",
        "BUTTON#9",
        "BUTTON#10",
        "BUTTON#11",
        "BUTTON#12",
        "BUTTON#13",
        "BUTTON#14",
        "BUTTON#15",
        "BUTTON#16",
        "BUTTON#17",
        "BUTTON#18",
        "BUTTON#19",
        "BUTTON#20"
    };

    private void addTriggers(final int buttonIndex) {
        if(cueArray[buttonIndex] == null) {
            Toast.makeText(this, "Cannot add triggers to empty sound cue.", Toast.LENGTH_SHORT).show();
            return;
        }

        mSelectedItems = new ArrayList<>();

        boolean[] isSelected = new boolean[triggers.length];

        for(int triggerIndex : cueArray[buttonIndex].triggers) {
            isSelected[triggerIndex] = true;
        }

        Dialog dialog = new AlertDialog.Builder(this)
            .setTitle(R.string.add_triggers_title)
            .setMultiChoiceItems(triggers, isSelected,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            mSelectedItems.add(which);
                        } else if (mSelectedItems.contains(which)) {
                            mSelectedItems.remove(Integer.valueOf(which));
                        }
                    }
                })
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    cueArray[buttonIndex].triggers.clear();
                    cueArray[buttonIndex].triggers.addAll(mSelectedItems);
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                }
            })
            .create();

        dialog.show();
    }

    private void deleteSound(int buttonIndex) {
        soundManager.remove(cueArray[buttonIndex]);
        cueArray[buttonIndex] = null;
    }

    private int getIndexForButton(View button) {
        int i = -1;
        switch (button.getId()) {
            case R.id.button11: i=0; break;
            case R.id.button12: i=1; break;
            case R.id.button13: i=2; break;
            case R.id.button14: i=3; break;
            case R.id.button21: i=4; break;
            case R.id.button22: i=5; break;
            case R.id.button23: i=6; break;
            case R.id.button24: i=7; break;
            case R.id.button31: i=8; break;
            case R.id.button32: i=9; break;
            case R.id.button33: i=10; break;
            case R.id.button34: i=11; break;
            case R.id.button41: i=12; break;
            case R.id.button42: i=13; break;
            case R.id.button43: i=14; break;
            case R.id.button44: i=15; break;
            case R.id.button51: i=16; break;
            case R.id.button52: i=17; break;
            case R.id.button53: i=18; break;
            case R.id.button54: i=19; break;
        }
        return i;
    }

    private Button getButtonForIndex(int index) {
        switch (index) {
            case 0: return button11;
            case 1: return button12;
            case 2: return button13;
            case 3: return button14;
            case 4: return button21;
            case 5: return button22;
            case 6: return button23;
            case 7: return button24;
            case 8: return button31;
            case 9: return button32;
            case 10: return button33;
            case 11: return button34;
            case 12: return button41;
            case 13: return button42;
            case 14: return button43;
            case 15: return button44;
            case 16: return button51;
            case 17: return button52;
            case 18: return button53;
            case 19: return button54;
        }
        return null;
    }

    private void assignOrPlay(final int index) {
        if(cueArray[index] == null) {
            assignIndex = index;
            showSoundFileChooser();
        }
        else {
            changeTextColor(index, Color.GREEN);
            soundManager.play(cueArray[index], new SoundManager.SoundListener() {
                @Override
                public void onPlayFinished() {
                    changeTextColor(index, 0xffcc2c00);
                }
            });

            if(cueArray[index].triggers != null) {
                for(final int triggerIndex : cueArray[index].triggers) {
                    if(cueArray[triggerIndex] != null)
                        soundManager.play(cueArray[triggerIndex]);
                }
            }
        }
    }

    @UiThread
    void changeTextColor(int index, int color) {
        getButtonForIndex(index).setTextColor(color);
    }

    private void play(int index) {
        if(cueArray[index] != null) {
            soundManager.play(cueArray[index]);
        }
    }

    public void showSoundFileChooser() {
        Intent intent = new Intent();
        intent.setType("audio/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Choose Sound File"), ASSIGN_SOUND_REQUEST);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;
        if ((event.getSource() & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD) {
            if (event.getRepeatCount() == 0) {
                Log.d(TAG, String.format("keyCode = %d, event = %s", keyCode, event.toString()));
                if(assignKeyOverlay.getVisibility() == View.VISIBLE) {
                    Log.d(TAG, "assign key for button index = " + assignButtonIndex);

                    cueArray[assignButtonIndex].assignedKey = keyCode;
                    refreshKeyBindings();

                    assignKeyOverlay.setVisibility(View.GONE);
                }
                else {
                    Log.d(TAG, "input key");
                    if(event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyBindings.containsKey(keyCode)) {
                            assignOrPlay(keyBindings.get(keyCode));
                        } else {
                            Log.w(TAG, "key not assigned");
                        }
                    }
                }
                handled = true;
            }
            if (handled) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void refreshKeyBindings() {
        keyBindings.clear();

        for(int i=0; i<cueArray.length; ++i) {
            if(cueArray[i] != null && cueArray[i].assignedKey != -1) {
                keyBindings.put(cueArray[i].assignedKey, i);
            }
        }
    }

    @OnActivityResult(ASSIGN_SOUND_REQUEST)
    void onAssignSoundRequestResult(int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            String filePath = data.getData().getPath();
            Log.d(TAG, "selected file = " + filePath + " assign index = " + assignIndex);
            if(assignIndex != -1) {
                cueArray[assignIndex] = new SoundCue(this, data.getData());
                soundManager.load(cueArray[assignIndex]);
                updateButtonText(assignIndex);
            }
        }
    }

    @OnActivityResult(EDIT_SOUND_REQUEST)
    void onEditSoundRequestResult(int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            int index = data.getIntExtra("index", -1);
            if(index != -1) {
                cueArray[index] = data.getParcelableExtra("cue");
                updateButtonText(index);
            }
        }
    }

    private void updateButtonText(int assignIndex) {
        String text = cueArray[assignIndex] == null ?
                getString(R.string.unassigned_button_text) :
                cueArray[assignIndex].file.getLastPathSegment();

        if(text.length() > 10) {
            text = text.substring(0, 10);
        }

        if(cueArray[assignIndex] != null && cueArray[assignIndex].assignedKey != -1) {
            text += "\n" + KeyUtils.keyCodeToString(cueArray[assignIndex].assignedKey);
        }

        Button update = getButtonForIndex(assignIndex);
        if(update != null) update.setText(text);
    }



    @Override
    public void onBackPressed() {
        if(assignKeyOverlay != null && assignKeyOverlay.getVisibility() == View.VISIBLE) {
            assignKeyOverlay.setVisibility(View.GONE);
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        soundManager.stop();

        // TODO save current layout to temp file
    }

    @Override
    protected void onResume() {
        super.onResume();

        // TODO load temp layout
    }

    boolean shouldReload = false;

    @Override
    protected void onStart() {
        super.onStart();

        // TODO ensure loaded soundboard is displayed

        if(soundManager.requiresReload()) {
            soundManager.load(cueArray);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        soundManager.release();
    }

    private enum CueOption {
        EDIT_CUE_OPTIONS,
        ASSIGN_KEY,
        ADD_TRIGGERS,
        DELETE_CUE;

        public static String[] names() {
            CueOption[] states = values();
            String[] names = new String[states.length];

            for (int i = 0; i < states.length; i++) {
                names[i] = states[i].name();
            }

            return names;
        }
    }

    public class UriTypeAdapter implements JsonSerializer<Uri>, JsonDeserializer<Uri> {
        @Override
        public JsonElement serialize(Uri src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }

        @Override
        public Uri deserialize(final JsonElement src, final Type srcType,
                               final JsonDeserializationContext context) throws JsonParseException {
            return Uri.parse(src.getAsString());
        }
    }
}
