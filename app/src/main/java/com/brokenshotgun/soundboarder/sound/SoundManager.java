package com.brokenshotgun.soundboarder.sound;

import android.content.Context;
import android.content.res.AssetManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.brokenshotgun.soundboarder.models.SoundCue;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.androidannotations.annotations.EBean.Scope.Singleton;

/**
 * Created by Jason Petterson on 1/3/2015.
 */
@EBean(scope = Singleton)
public class SoundManager {
    private static final String TAG = SoundManager.class.getName();
    public static final int MAX_STREAMS = 20;

    @RootContext
    Context context;

    private final Map<SoundCue, Integer> soundIdMap = Collections.synchronizedMap(new HashMap<SoundCue, Integer>());
    private final Map<Integer, Boolean> playingMap = Collections.synchronizedMap(new HashMap<Integer, Boolean>());
    private SoundPool soundPool = null;

    @SuppressWarnings("deprecation")
    @AfterInject
    void init() {
        soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
    }

    public static interface SoundListener {
        public void onPlayFinished();
    }

    public void load(SoundCue... cues) {
        for(SoundCue cue : cues) {
            load(cue);
        }

        shouldReload = false;
    }

    public void load(SoundCue cue) {
        if(cue == null) return;

        if(soundPool == null) {
            init();
        }

        int soundId = soundPool.load(cue.file.getPath(), 1);
        soundIdMap.put(cue, soundId);
        playingMap.put(soundId, false);
    }

    public void play(final SoundCue cue) {
        play(cue, null);
    }

    @Background
    public void play(final SoundCue cue, final SoundListener listener) {
        if(isPlaying(cue)) {
            pauseCue(cue);
            if(listener != null) listener.onPlayFinished();
            return;
        }

        for(SoundCue c : soundIdMap.keySet()) {
            if(c.priority < cue.priority) {
                pauseCue(c);
            }
        }

        soundPool.play(soundIdMap.get(cue), cue.volume, cue.volume, cue.priority, cue.loop, 1.0f);
        playingMap.put(soundIdMap.get(cue), true);

        if(cue.loop >= 0) {
            try {
                Thread.sleep(cue.duration * (cue.loop + 1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                playingMap.put(soundIdMap.get(cue), false);
                if(listener != null) listener.onPlayFinished();
            }
        }
    }

    private void pauseCue(SoundCue cue) {
        soundPool.pause(soundIdMap.get(cue));
        playingMap.put(soundIdMap.get(cue), false);
    }

    private boolean isPlaying(SoundCue cue) {
        return soundIdMap.containsKey(cue) && playingMap.get(soundIdMap.get(cue));
    }

    public void stop() {
        if(soundPool != null) {
            soundPool.autoPause();
        }
    }

    public void remove(SoundCue cue) {
        if(cue == null) return;

        if(soundPool != null) {
            if(soundIdMap.containsKey(cue)) {
                int soundId = soundIdMap.get(cue);
                soundPool.stop(soundId);
                soundIdMap.remove(cue);
                playingMap.remove(soundId);
            }
        }
    }

    private boolean shouldReload = false;

    public boolean requiresReload() {
        return shouldReload;
    }

    public void release() {
        if(soundPool != null) {
            soundPool.release();
            soundPool = null;

            soundIdMap.clear();
            playingMap.clear();

            shouldReload = true;
        }
    }
}
